"""chub URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from charity import views
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('admin/', admin.site.urls),
    path("",views.new,name="home"),
    path("about",views.about,name="about"),
    path("contact",views.Contact,name="contact"),
    path("signin",views.loginpage,name="signin"),
    path("signup",views.logup,name="signup"),
    path("check_user/",views.check_user,name="check_user"),
    path("user_login/",views.user_login,name="user_login"),
    path("custdashbord/",views.cust_dashboard,name="cust_dashboard"),
    path("user_logout",views.user_logout,name="user_logout"),
    path("edit_profile",views.edit_profile,name="edit_profile"),
    path('donate/',views.donate,name="donate"),
    path('success_payment/',views.success_payment,name="success_payment"),
    path('scope/',views.scope,name="scope"),
    path("sendmail/",views.sendmail,name="sendmail"),
    path("change_password",views.change_password,name="change_password"),
    path("forgotpass",views.forgotpass,name="forgot_pass"),
    path("reset_password",views.reset_password,name="reset_password"),
    path('become_volunteer/',views.become_volunteer,name="become_volunteer"),
    path('paypal/', include('paypal.standard.ipn.urls')),
    
    


]+static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)

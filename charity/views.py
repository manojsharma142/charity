from django.shortcuts import render,get_object_or_404,reverse
from django.http import HttpResponse, HttpResponseRedirect,JsonResponse
from charity.models import contact,category,register_table, volunteer,work, workImages,donor,doner
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.core.mail  import EmailMessage
from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings





works = work.objects.all().order_by('title')
images = workImages.objects.all()
volunt = volunteer.objects.filter(status=True).order_by('name')
support = []
for i in works:
    w = {}
    w['id'] = i.id
    w['title']=i.title
    w['description1']=i.description1[:120]
    support.append(w)


def new(request):
    works = work.objects.all().order_by('title')
    cats = donor.objects.all().order_by("name")
    return render(request,"firstpage.html",{'donor':cats,'support':support,'vol':volunt,'works':works,})
def about(request):
    return render(request,'about.html',{'works':works,'gallery':gallery})
def Contact(request):
     alldata = contact.objects.all().order_by("-id")
    
     if request.method=="POST":
        nm=request.POST["name"]
        con=request.POST["contact"]
        sub=request.POST["subject"]
        msz=request.POST["message"]


        data = contact(name=nm,contact_number=con,subject=sub,message=msz)
        data.save()
        res = "Dear {} Thanks for your Feedback".format(nm)
        #return HttpResponse("<h1>Data Saved Successfully</h1>")
        return render(request,"contact.html",{"status":res,"message":alldata})
    
     return render(request,"contact.html",{"message":alldata,'works':works,})
     
        
def loginpage(request):
    return render(request,"signin.html")
def logup(request):
     if request.method=="POST":
        fname = request.POST["first"]
        usernm =request.POST["uname"] 
        last =  request.POST["last"]
        pwd =   request.POST["password1"]
        conf =   request.POST["password2"]  
        em   =   request.POST["email"]
        con  =  request.POST["contact"]
     
        usr = User.objects.create_user(usernm,em,pwd)
        usr.username = usernm 
        usr.first_name = fname
        usr.last_name = last
        usr.save()
        reg = register_table(user=usr,contact_number=con)
        reg.save()


        
     return render(request,"signup.html")

# def check_user(request):
#     if request.method=="GET":
#         un = request.GET["usern"]
#         check = User.objects.filter(username=un)
#         if len(check) == 1:
#            return HttpResponse("Exists")
#         else:
#             return HttpResponse("Not Exists")

def check_user(request):
    if request.method=="GET":
        un = request.GET["usern"]
        check = User.objects.filter(username=un)
        if len(check) == 1:
            return HttpResponse("Exists")
        else:
            return HttpResponse("Not exists")    

def user_login(request):
    if request.method=="POST":
        un = request.POST["username"]
        pwd =request.POST["password"]

        #user = authenticate(username=un,password=pwd) 
        #return HttpResponse(user)

        user = authenticate(username=un,password=pwd)
        if user:
            login(request,user)
            if user.is_superuser:
                return HttpResponseRedirect("/admin")
            if user.is_staff:
                #return HttpResponseRedirect("/instructor_profile")
               return HttpResponse("<h1 style='color:red;'>welcome {} to patient Dashboard </h1>".format(user.first_name))
            if user.is_active:
                #return HttpResponseRedirect("/wish")
                #return HttpResponse("<h1 style='color:green;'>welcome {} to Donar Dashboard </h1>".format(user.first_name))
                return HttpResponseRedirect("/custdashbord")   
        else:
            return render(request,"firstpage.html",{"status":"Invalid Username or Password"})


    
    
    return HttpResponse("called")

@login_required
def cust_dashboard(request):
    data = register_table.objects.get(user__id = request.user.id)
    works =work.objects.all()
    if 'name' in request.GET:
        name = request.GET['name']
        em = request.GET['email']
        cont = request.GET['cont']
        coun = request.GET['coun']
        st = request.GET['st']
        ct = request.GET['ct']
        add1 = request.GET['add1']
        add2 = request.GET['add2']
        amt = request.GET['amt']
        rem = request.GET['rem']
        wrk = request.GET['work']

        dn = donor(name=name,email=em,contact=cont,country=coun,state=st,city=ct,address_line1=add1,address_line2=add2,amount=amt,remarks=rem,work=wrk)
        dn.save()
       
        return HttpResponse(dn.id)
    return render(request,"custdashbord.html",{"data":data,"works":works})





def user_logout(request):
    logout(request)
    return HttpResponseRedirect("/")    

def edit_profile(request):
    context = {}
    data = register_table.objects.get(user__id = request.user.id)
    context["data"]=data
    if request.method == "POST":
        fn = request.POST["fname"]
        ln = request.POST["lname"]
        em = request.POST["email"]
        con = request.POST["contact"]
        age = request.POST["age"]
        ct = request.POST["city"]
        gen = request.POST["gender"]
        occ = request.POST["occupation"]
        abt = request.POST["about"]

        usr = User.objects.get(id=request.user.id)
        usr.first_name = fn
        usr.last_name = ln
        usr.email = em
        usr.save()


        data.contact_number = con
        data.age = age
        data.city = ct
        data.gender = gen
        data.occupation = occ
        data.about = abt
        data.save()

        if "image" in request.FILES:
            img = request.FILES["image"]
            data.profile_pic = img
            data.save()

        context["status"] = "Changes Saved Successfully"

    return render(request,"edit_profile.html",context)
def donate(request):
    if 'name' in request.GET:
        name = request.GET['name']
        em = request.GET['email']
        cont = request.GET['cont']
        coun = request.GET['coun']
        st = request.GET['st']
        ct = request.GET['ct']
        add1 = request.GET['add1']
        add2 = request.GET['add2']
        amt = request.GET['amt']
        rem = request.GET['rem']
        work = request.GET['work']

        data = donor(name=name,email=em,contact=cont,country=coun,state=st,city=ct,address_line1=add1,address_line2=add2,amount=amt,remarks=rem,work=work)
        data.save()
        return HttpResponse(data.id)
    return render(request,'custdashbord.html',{'works':works})
def success_payment(request):
    if 'ORDERID' in request.GET:
        status = request.GET['STATUS']
        if status == "TXN_FAILURE":
            return HttpResponseRedirect('/donate/')
        order_id = request.GET['ORDERID']
        txn_id = request.GET['TXNID']
        pay_mode = request.GET['PAYMENTMODE']
        bank_name = request.GET['BANKNAME']

        sid = order_id.split('o')[0]
        order_obj = donor.objects.get(id=int(sid))
        order_obj.txn_id = txn_id
        order_obj.payment_method= pay_mode
        order_obj.bank_name = bank_name
        order_obj.status = True
        order_obj.save()

        return render(request,'process.html',{'txn_id':txn_id,'works':works,'amount':order_obj.amount,'name':order_obj.name})
def become_volunteer(request):
    if request.method=="POST":
        status = ''
        name = request.POST['name']
        email = request.POST['email']
        contact = request.POST['contact']
        country = request.POST['country']
        state = request.POST['state']
        city = request.POST['city']
        address_line1 = request.POST['address1']
        address_line2 = request.POST['address2']
        age = request.POST['age']
        qual = request.POST['qualification']
        rem = request.POST['remarks']
        
        data = volunteer.objects.filter(contact=contact)
        if len(data)>=1:
            status = 'A Volunteer with this mobile number already exists!!!'
        else:
            data=volunteer(name=name,email=email,contact=contact,country=country,state=state,city=city,address_line1=address_line1,address_line2=address_line2,age=age,qualification=qual,other_information=rem,status=True)
            data.save()
            if 'profile_pic' in request.FILES:
                pp = request.FILES['profile_pic']
                data.profile_pic = pp
                data.save()
            status = "Dear {} welcome to Charity Hub, We will soon contact you if any charity event in your area".format(name)
            return render(request,'volunteer.html',{'status':status,'works':works})
            
    return render(request,'volunteer.html',{'works':works})


def scope(request):
    id = request.GET['id']
    works = work.objects.all().order_by("title")
    data = work.objects.get(pk=id)
    images = workImages.objects.all()
    return render(request,'scope.html',{'works':works,'work':data,'images':images})



# def sendmail(request):
#     context = {}
#     ch = register_table.objects.filter(user__id=request.user.id)
#     if len(ch)>0:
#         data = register_table.objects.get(user__id=request.user.id)
#         context["data"] = data
    
#     if request.method=="POST":
        
        

#         rec = request.POST["to"].split(",")
#         print(rec)
#         sub = request.POST["sub"]
#         msz = request.POST["msz"]
#         try:

#             em = EmailMessage(sub,msz,to=rec) 
#             em.send()
#             context["status"] = "Email Sent"
#             context["cls"] = "alert-success"

#         except:
#             context["status"] = "Could Not Send Email, Please check Internet Connection / Email Address"
#             context["cls"] = "alert-danger"

#     return render(request,"sendmail.html",context)   
   
def sendmail(request):
    context = {}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    
    if request.method=="POST":
        
        

        rec = request.POST["to"].split(",")
        print(rec)
        sub = request.POST["sub"]
        msz = request.POST["msz"]
        try:

            em = EmailMessage(sub,msz,to=rec) 
            em.send()
            context["status"] = "Email Sent"
            context["cls"] = "alert-success"

        except:
            context["status"] = "Could Not Send, Please check Internet Connection / Email Address"
            context["cls"] = "alert-danger"

    return render(request,"sendmail.html",context)   



def change_password(request):
    context={}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    if request.method=="POST":
        current = request.POST["cpwd"]
        new_pas = request.POST["npwd"]
        
        user = User.objects.get(id=request.user.id)
        un = user.username
        check = user.check_password(current)
        if check==True:
            user.set_password(new_pas)
            user.save()
            context["msz"] = "Password Changed Successfully!!!"
            context["col"] = "alert-success"
            user = User.objects.get(username=un)
            login(request,user)
        else:
            context["msz"] = "Incorrect Current Password"
            context["col"] = "alert-danger"

    return render(request,"reset_password.html",context)
gallery = []
for i in images:
    gallery.append(i.image1)
    gallery.append(i.image2)
    gallery.append(i.image3)
    gallery.append(i.image4)
    

import random


def forgotpass(request):
    context = {}
    if request.method=="POST":
        un = request.POST["username"]
        pwd = request.POST["npass"]

        user = get_object_or_404(User,username=un)
        user.set_password(pwd)
        user.save()

           
        context["status"] = "Password Change Successfully!!!"
    return render(request,"forgot_pass.html",context)

import random

def reset_password(request):
    un = request.GET["username"]
    try:
        user = get_object_or_404(User,username=un) 
        otp = random.randint(1000,9999)
        msz = "Dear {} \n{} is your OTP \nDo not share with anyone \nThanku \nCharityHub".format(user.username,otp)
        try:
            email = EmailMessage("Account Verification",msz,to=[user.email])
            email.send()
            return JsonResponse({"status":"sent","email":user.email,"rotp":otp})

        except:
            return JsonResponse({"status":"Error","email":user.email})
      
        
    except:
        return JsonResponse({"status":"failed"})





from django.db import models
from django.contrib.auth.models import User
class contact(models.Model):
    name = models.CharField(max_length=250)
    contact_number = models.IntegerField(blank=True,unique=True)
    subject = models.CharField(max_length=250)
    message = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.name


    class Meta:
        verbose_name_plural = "contact"   

class category(models.Model):
    name = models.CharField(max_length=250)
    cover_pic = models.FileField(upload_to = "media/%Y/%m/%d")
    description = models.TextField()
    added_on = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Category"  





class register_table(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact_number = models.IntegerField()
    profile_pic = models.ImageField(upload_to = "profiles/%y/%m/%d",null=True)
    age = models.CharField(max_length=250,null=True,blank=True)
    city = models.CharField(max_length=250,null=True,blank=True)
    occupation = models.CharField(max_length=250,null=True,blank=True)
    about = models.TextField(null=True,blank=True)
    gender = models.CharField(max_length=250,null=True,blank=True)
    added_on = models.DateTimeField(auto_now_add=True,null=True,)
    update_on =  models.DateTimeField(auto_now=True,null=True,)
    def __str__(self):
        return self.user.username

class volunteer(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200,blank=True)
    contact = models.IntegerField(null=True,blank=True)
    country = models.CharField(max_length=200,blank=True)
    state = models.CharField(max_length=200,blank=True)
    city = models.CharField(max_length=200,blank=True)
    address_line1 = models.CharField(max_length=500,blank=True)
    address_line2 = models.CharField(max_length=500,blank=True)
    age = models.IntegerField(null=True,blank=True)
    qualification = models.CharField(max_length=300,blank=True)
    profile_pic=models.ImageField(upload_to="volunteers/%Y/%m/%d")
    other_information = models.TextField(blank=True)
    on_date = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.name
class work(models.Model):
    title=models.CharField(max_length=200)
    description1 = models.TextField()
    description2 = models.TextField(blank=True)
    description3 = models.TextField(blank=True)
    added_on = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.title

class workImages(models.Model):
    work = models.ForeignKey(work,on_delete=models.CASCADE)
    image1 = models.ImageField(upload_to="works/%Y/%m/%d",blank=True)
    image2 = models.ImageField(upload_to="works/%Y/%m/%d",blank=True)
    image3 = models.ImageField(upload_to="works/%Y/%m/%d",blank=True)
    image4 = models.ImageField(upload_to="works/%Y/%m/%d",blank=True)
    added_on = models.DateField(auto_now_add=True)
    def __str__(self):
        return self.work.title

   
class donor(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200,blank=True)
    contact = models.IntegerField(null=True,blank=True)
    country = models.CharField(max_length=200,blank=True)
    state = models.CharField(max_length=200,blank=True)
    city = models.CharField(max_length=200,blank=True)
    address_line1 = models.CharField(max_length=500,blank=True)
    address_line2 = models.CharField(max_length=500,blank=True)
    amount = models.DecimalField(max_digits=10,decimal_places=2)
    remarks = models.TextField(blank=True)
    work = models.CharField(max_length=300,blank=True)
    payment_method=models.CharField(max_length=200,blank=True)
    txn_id = models.CharField(max_length=300,blank=True)
    on_date = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.name

class doner(models.Model):
    user =models.ForeignKey(User,on_delete = models.CASCADE)
    email = models.EmailField(max_length=200,blank=True)
    contact = models.IntegerField(null=True,blank=True)
    country = models.CharField(max_length=200,blank=True)
    state = models.CharField(max_length=200,blank=True)
    city = models.CharField(max_length=200,blank=True)
    address_line1 = models.CharField(max_length=500,blank=True)
    address_line2 = models.CharField(max_length=500,blank=True)
    amount = models.DecimalField(max_digits=10,decimal_places=2)
    remarks = models.TextField(blank=True)
    work = models.CharField(max_length=300,blank=True)
    payment_method=models.CharField(max_length=200,blank=True)
    txn_id = models.CharField(max_length=300,blank=True)
    on_date = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=False)
   
    def __str__(self):
        return self.user.username
  
    # Create your models here.

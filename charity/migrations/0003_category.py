# Generated by Django 3.0.4 on 2020-04-14 11:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('charity', '0002_auto_20200412_1832'),
    ]

    operations = [
        migrations.CreateModel(
            name='category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('cover_pic', models.FileField(upload_to='media/%y/%m/%d')),
                ('description', models.TextField()),
                ('added_on', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'Media',
            },
        ),
    ]

from django.contrib import admin
from charity.models import contact,category,register_table,volunteer,work,workImages,donor,doner
admin.site.site_header="CharityHub"

class contactAdmin(admin.ModelAdmin):
    list_display = ["id","name","contact_number","added_on"]
    search_fields=["name"]
class categoryAdmin(admin.ModelAdmin):
    list_display = ["id","name","description","cover_pic","added_on"]
class volunteerAdmin(admin.ModelAdmin):
    list_display = ['name','email','country','state','city','qualification','age','on_date']
class workAdmin(admin.ModelAdmin):
    list_display = ['id','title','description1','description2','description3','added_on']
class donorAdmin(admin.ModelAdmin):
    list_display = ['name','txn_id','email','country','state','city','amount','work','on_date']


admin.site.register(contact,contactAdmin)
admin.site.register(category,categoryAdmin)
admin.site.register(register_table)
admin.site.register(volunteer,volunteerAdmin)
admin.site.register(work,workAdmin)
admin.site.register(workImages)
admin.site.register(donor,donorAdmin)
admin.site.register(doner,)
# Register your models here.
